import shutil
import os
import copy
import json

import pytest
from fastapi.testclient import TestClient

from mmui.api.v1.prom.router import router as app
from mmui.api.v1.prom.models import Config
from mmui.config import (
    DATA_PATH,
    PROMETHEUS_CONFIG_PATH,
    NGINX_CONFIG_PATH,
    GRAFANA_CONFIG_PATH
)
from mmui.initialize import init


client = TestClient(app)


@pytest.fixture
def remove_env(autouse=True):
    for dir in [DATA_PATH, PROMETHEUS_CONFIG_PATH, NGINX_CONFIG_PATH, GRAFANA_CONFIG_PATH]:
        if os.path.exists(dir):
            shutil.rmtree(dir)


@pytest.fixture
def default_config():
    init()
    return Config.from_file()
    

@pytest.fixture
def default_config_json(default_config):
    init()
    config = default_config.model_dump_json(by_alias=True)
    return config


@pytest.fixture
def default_scrape_config_json(default_config):
    init()
    config = default_config.model_dump_json(by_alias=True)
    return config


def test_get_config(default_config_json):
    init()

    response = client.get('/config')

    assert response.status_code == 200
    assert response.text == default_config_json


def test_reset_config(default_config):
    init()

    modified_config = copy.deepcopy(default_config)
    modified_config.global_.scrape_interval = '666s'
    modified_config.save()

    response = client.post('/config/reset')

    assert Config.from_file() == default_config
    assert response.status_code == 200


def test_config_set(default_config_json):
    init()
    modified_config = json.loads(default_config_json)
    modified_config['global']['scrape_interval'] = '666s'

    response = client.put('/config/set', content=json.dumps(modified_config))

    assert Config.from_file().model_dump(by_alias=True) == modified_config
    assert response.status_code == 200


def test_add_scrape_config():
    init()
    scrape_config = json.dumps({'job_name': 'test-job', 'scrape_configs': [{'targets': 'testaddress:6666'}]})

    response = client.post('/config/scrapes/scrape', content=scrape_config)

    success = False
    for sc_config in Config.from_file().model_dump(by_alias=True)['scrape_configs']:
        if sc_config['job_name'] == 'test-job':
            success = True
    assert success
    assert response.status_code == 200


def test_delete_scrape_config():
    init()

    response = client.delete('/config/scrapes/scrape', params={'job_name': 'prometheus'})

    assert not ('prometheus' in Config.from_file().model_dump(by_alias=True))
    assert response.status_code == 200


def test_get_health():
    response = client.get('/health')
    assert response.status_code == 200


def test_get_readiness():
    response = client.get('/readiness')
    assert response.status_code == 200


def reload_service():
    response = client.get('/reload')
    assert response.status_code == 200


import shutil

from fastapi.testclient import TestClient

from mmui.app import app
from mmui.config import DATA_PATH
from mmui.initialize import init


client = TestClient(app)


def test_check_health():
    init()
    response = client.get('/health')
    assert response.status_code == 200

    shutil.rmtree(DATA_PATH)
    response = client.get('/health')
    assert response.status_code == 503


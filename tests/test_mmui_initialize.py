import os

import pytest
import yaml
from yaml.loader import SafeLoader

from mmui.initialize import (
    init,
    init_data,
    init_prom,
    init_nginx,
    init_grafana,
    initialized
)


URL = 'http://localhost:8000'
LISTEN_ADDR = '0.0.0.0'
LISTEN_PORT = '8000'
PROMETHEUS_URL = 'http://localhost:9090'
GRAFANA_URL = 'http://localhost:3000'
GRAFANA_CONFIG_FILE = '''
[server]
root_url = %(protocol)s://%(domain)s:%(http_port)s/grafana/
serve_from_sub_path = true '''
NGINX_URL = 'http://localhost:8080'
NGINX_CONFIG_FILE = f'''
upstream grafana {{
  server {GRAFANA_URL.split('//')[1]};
}}
upstream prometheus {{
  server {PROMETHEUS_URL.split('//')[1]};
}}
upstream mmui {{
  server {URL.split('//')[1]};
}}
# this is required to proxy Grafana Live WebSocket connections.
map $http_upgrade $connection_upgrade {{
  default upgrade;
  '' close;
}}
server {{
  listen {NGINX_URL.split(':')[2]};
  location /gateway-health {{
    return 200;
    access_log off;
    add_header Content-Type text/plain;
  }}
  location / {{
    proxy_pass http://mmui;
  }}
  location /grafana/ {{
    proxy_set_header Host $http_host;
    proxy_pass http://grafana;
  }}
  # Proxy Grafana Live WebSocket connections.
  location /grafana/api/live/ {{
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_set_header Host $http_host;
    proxy_pass http://grafana;
  }}
  location /prometheus/ {{
    proxy_pass http://prometheus;
  }}
}}
'''


@pytest.fixture
def data_path(tmp_path):
    return os.path.join(tmp_path, 'data')

@pytest.fixture
def nginx_path(data_path):
    return os.path.join(data_path, 'nginx')

@pytest.fixture
def prometheus_path(data_path):
    return os.path.join(data_path, 'prometheus')

@pytest.fixture
def grafana_path(data_path):
    return os.path.join(data_path, 'grafana')


def test_init_data(data_path):
    init_data(data_path)

    assert os.path.exists(data_path)
    assert os.path.exists(os.path.join(data_path, 'initialized'))
    assert os.path.isfile(os.path.join(data_path, 'initialized'))


def test_initialized(data_path):
    assert initialized(data_path) == False
    init_data(data_path)
    assert initialized(data_path) == True


def test_init_nginx(data_path, nginx_path):
    init_nginx(data_path, nginx_path, NGINX_CONFIG_FILE)

    assert os.path.exists(nginx_path)
    assert os.path.exists(os.path.join(nginx_path, 'default.conf'))
    assert os.path.isfile(os.path.join(nginx_path, 'default.conf'))
    with open(os.path.join(nginx_path, 'default.conf'), 'r') as conf_file:
        assert conf_file.read() == NGINX_CONFIG_FILE


def test_init_grafana(data_path, grafana_path):
    init_grafana(data_path, grafana_path, GRAFANA_CONFIG_FILE)

    assert os.path.exists(grafana_path)
    assert os.path.exists(os.path.join(grafana_path, 'grafana.ini'))
    assert os.path.isfile(os.path.join(grafana_path, 'grafana.ini'))
    with open(os.path.join(grafana_path, 'grafana.ini'), 'r') as conf_file:
        assert conf_file.read() == GRAFANA_CONFIG_FILE


def test_init_prom(data_path, prometheus_path):
    init_prom(data_path, prometheus_path, GRAFANA_URL, PROMETHEUS_URL)

    assert os.path.exists(prometheus_path)
    assert os.path.exists(os.path.join(prometheus_path, 'prometheus.yml'))
    assert os.path.isfile(os.path.join(prometheus_path, 'prometheus.yml'))
    with open(os.path.join(prometheus_path, 'prometheus.yml'), 'r') as file:
        prom_config_data = yaml.load(file, Loader=SafeLoader)
        assert 'global' in prom_config_data
        assert 'scrape_configs' in prom_config_data
        assert len(prom_config_data['scrape_configs']) == 2


def test_init(data_path, prometheus_path, grafana_path, nginx_path):
    assert initialized(data_path) == False
    init(
        data_path=data_path,
        prometheus_config_path=prometheus_path,
        prometheus_url=PROMETHEUS_URL,
        grafana_url=GRAFANA_URL,
        grafana_config_path=grafana_path,
        grafana_config_file=GRAFANA_CONFIG_FILE,
        nginx_config_path=nginx_path,
        nginx_config_file=NGINX_CONFIG_FILE,
    )
    assert initialized(data_path) == True

    for path in [nginx_path, grafana_path, prometheus_path]:
        assert os.path.exists(path)


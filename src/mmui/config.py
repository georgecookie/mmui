import os


URL = os.getenv('MMUI_URL', 'http://localhost:8000')
LISTEN_ADDR = os.getenv('MMUI_LISTEN_ADDR', '0.0.0.0')
LISTEN_PORT = os.getenv('MMUI_LISTEN_PORT', '8000')
RELOAD_ON_CHANGE = os.getenv('MMUI_RELOAD_ON_CHANGE', False)
PROXY_SUBPATH = os.getenv('MMUI_PROXY_SUBPATH', '')

DATA_PATH = os.getenv('MMUI_DATA_PATH', os.path.join('/tmp', 'mmui_data'))


PROMETHEUS_URL = os.getenv('MMUI_PROMETHEUS_URL', 'http://localhost:9090')
PROMETHEUS_CONFIG_PATH = os.getenv('MMUI_PROMETHEUS_CONFIG_PATH', os.path.join(DATA_PATH, 'prometheus'))


GRAFANA_URL = os.getenv('MMUI_GRAFANA_URL', 'http://localhost:3000')
GRAFANA_CONFIG_PATH = os.getenv('MMUI_GRAFANA_CONFIG_PATH', os.path.join(DATA_PATH, 'grafana'))
GRAFANA_CONFIG_FILE = '''
[server]
root_url = %(protocol)s://%(domain)s:%(http_port)s/grafana/
serve_from_sub_path = true '''


NGINX_URL = os.getenv('MMUI_NGINX_URL', 'http://localhost:8080')
NGINX_CONFIG_PATH = os.getenv('MMUI_NGINX_CONFIG_PATH', os.path.join(DATA_PATH, 'nginx'))
NGINX_CONFIG_FILE = f'''
upstream grafana {{
  server {GRAFANA_URL.split('//')[1]};
}}
upstream prometheus {{
  server {PROMETHEUS_URL.split('//')[1]};
}}
upstream mmui {{
  server {URL.split('//')[1]};
}}
# this is required to proxy Grafana Live WebSocket connections.
map $http_upgrade $connection_upgrade {{
  default upgrade;
  '' close;
}}
server {{
  listen {NGINX_URL.split(':')[2]};
  location /gateway-health {{
    return 200;
    access_log off;
    add_header Content-Type text/plain;
  }}
  location / {{
    proxy_pass http://mmui;
  }}
  location /grafana/ {{
    proxy_set_header Host $http_host;
    proxy_pass http://grafana;
  }}
  # Proxy Grafana Live WebSocket connections.
  location /grafana/api/live/ {{
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $connection_upgrade;
    proxy_set_header Host $http_host;
    proxy_pass http://grafana;
  }}
  location /prometheus/ {{
    proxy_pass http://prometheus;
  }}
}}
'''


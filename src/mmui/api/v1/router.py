from fastapi import APIRouter

from .prom import router as prom_router


router = APIRouter()

router.include_router(router=prom_router.router, prefix='/prom', tags=['API V1 Prometheus'])


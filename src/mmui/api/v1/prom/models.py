import os
from enum import Enum
from typing_extensions import Annotated

import yaml
from yaml.loader import SafeLoader

from pydantic import (
    ConfigDict,
    BaseModel,
    AfterValidator,
    Field,
    SecretStr,
    field_serializer
)

from mmui.config import PROMETHEUS_CONFIG_PATH
from .validators import duration, labelname, size


Duration = Annotated[str, AfterValidator(duration)]
LabelName = Annotated[str, AfterValidator(labelname)]
Size = Annotated[int | str, AfterValidator(size)]


class PromStatusMessage(BaseModel):
    ''' Prometheus status message scheme. '''
    status: int
    message: str


class Scheme(str, Enum):
    ''' Prometheus 'scheme' type '''
    http = 'http'
    https = 'https'


class TlsVersion(str, Enum):
    ''' Prometheus 'tls_version' variation '''
    tls10 = 'TLS10'
    tls11 = 'TLS11'
    tls12 = 'TLS12'
    tls13 = 'TLS13'


class RelabelAction(str, Enum):
    ''' Prometheus 'relabel_action' type '''
    REPLACE = 'replace'
    LOWERCASE = 'lowercase'
    UPPERCASE = 'uppercase'
    KEEP = 'keep'
    DROP = 'drop'
    KEEPEQUAL = 'keepequal'
    DROPEQUAL = 'dropequal'
    HASHMOD = 'hasmod'
    LABELMAP = 'labelmap'
    LABELDROP = 'labeldrop'
    LABELKEEP = 'labelkeep'


class KubernetesSDRole(str, Enum):
    ''' Pormetheus config Kubernetes SD role setting scheme '''
    endpoints = 'endpoints'
    endpointslice = 'endpointslice'
    service = 'service'
    pod = 'pod'
    node = 'node'
    ingress = 'ingress'


class DockerSDFilter(BaseModel):
    ''' Pormetheus config Docker SD filter setting scheme '''
    name: str
    values: list[str]


class TlsConfig(BaseModel):
    ''' Prometheus 'tls_config' type '''
    ca: str | None = None
    cert: str
    key: SecretStr
    server_name: str | None = None
    insecure_skip_verify: bool = False
    min_version: TlsVersion | None = None
    max_version: TlsVersion | None = None

    @field_serializer('key', when_used='json')
    def dump_secret(self, v):
        return v.get_secret_value()


class BasicAuth(BaseModel):
    ''' Pormetheus config basic_auth setting scheme '''
    username: str
    password: SecretStr

    @field_serializer('password', when_used='json')
    def dump_secret(self, v):
        return v.get_secret_value()


class Oauth2(BaseModel):
    ''' Pormetheus config oauth2 setting scheme '''
    client_id: str
    client_secret: SecretStr
    scopes: list[str]
    token_url: str
    endpoint_params: dict[str, str] = {}
    tls_config: TlsConfig | None = None
    proxy_url: str | None = None
    no_proxy: str | None = None
    proxy_from_environment: bool = False
    proxy_connect_header: dict[str, list[str]] | None = None

    model_config = ConfigDict(use_enum_values=True, validate_default=True)

    @field_serializer('client_secret', when_used='json')
    def dump_secret(self, v):
        return v.get_secret_value()


class Authorization(BaseModel):
    ''' Pormetheus config authorization setting scheme '''
    type_: str = Field('Bearer', alias='type')
    credentials: SecretStr

    @field_serializer('credentials', when_used='json')
    def dump_secret(self, v):
        return v.get_secret_value()


class KubernetesNamespaceSelector(BaseModel):
    ''' Pormetheus config Kubernetes SD 'namespaces' setting scheme '''
    own_namespace: bool = True
    names: list[str] | None = None


class KubernetesSDSelectors(BaseModel):
    ''' Pormetheus config Kubernetes SD 'selectors' setting item '''
    role: KubernetesSDRole
    label: str | None = None
    field: str | None = None


class KubernetesSDConfig(BaseModel):
    ''' Pormetheus config kubernetes_sd_config type '''
    api_server: str
    role: KubernetesSDRole
    namespaces: KubernetesNamespaceSelector | None = None
    selectors: list[KubernetesSDSelectors] | None = None
    basic_auth: BasicAuth | None = None
    tls_config: TlsConfig | None = None
    authorization: Authorization | None = None
    oauth2: Oauth2 | None = None
    proxy_url: str | None = None
    no_proxy: str | None = None
    proxy_from_environment: bool = False
    proxy_connect_header: dict[str, list[str]] | None = None
    follow_redirects: bool = True
    enable_http2: bool = True

    model_config = ConfigDict(use_enum_values=True, validate_default=True)


class GceSDConfig(BaseModel):
    ''' Pormetheus config gce_sd_config type '''
    project: str
    zone: str
    filter: str | None = None
    refresh_interval: Duration = '60s'
    port: int = 80
    tag_separator: str = ','


class HttpSDConfig(BaseModel):
    ''' Pormetheus config http_sd_config type '''
    url: str
    refresh_interval: Duration = '60s'
    tls_config: TlsConfig | None = None
    basic_auth: BasicAuth | None = None
    authorization: Authorization | None = None
    oauth2: Oauth2 | None = None
    follow_redirects: bool = True
    enable_http2: bool = True
    proxy_url: str | None = None
    no_proxy: str | None = None
    proxy_from_environment: bool = False
    proxy_connect_header: dict[str, list[str]] | None = None

    model_config = ConfigDict(use_enum_values=True, validate_default=True)


class AzureSDConfig(BaseModel):
    ''' Pormetheus config azure_sd_config type '''
    environment: str = 'AzurePublicCloud'
    authentication_method: str = 'OAuth'
    subscription_id: str
    tenant_id: str
    client_id: str
    client_secret: SecretStr
    resource_group: str
    refresh_interval: Duration = '60s'
    tls_config: TlsConfig | None = None
    basic_auth: BasicAuth | None = None
    authorization: Authorization | None = None
    oauth2: Oauth2 | None = None
    follow_redirects: bool = True
    enable_http2: bool = True
    proxy_url: str | None = None
    no_proxy: str | None = None
    proxy_from_environment: bool = False
    proxy_connect_header: dict[str, list[str]] | None = None

    model_config = ConfigDict(use_enum_values=True, validate_default=True)

    @field_serializer('client_secret', when_used='json')
    def dump_secret(self, v):
        return v.get_secret_value()


class DockerSDConfig(BaseModel):
    ''' Pormetheus config docker_sd_config type '''
    host: str
    port: int = 80
    host_networking_host: str
    filters: list[DockerSDFilter] | None = None
    refresh_interval: Duration = '60s'
    tls_config: TlsConfig | None = None
    basic_auth: BasicAuth | None = None
    authorization: Authorization | None = None
    oauth2: Oauth2 | None = None
    follow_redirects: bool = True
    enable_http2: bool = True
    proxy_url: str | None = None
    no_proxy: str | None = None
    proxy_from_environment: bool = False
    proxy_connect_header: dict[str, list[str]] | None = None

    model_config = ConfigDict(use_enum_values=True, validate_default=True)


class StaticConfig(BaseModel):
    ''' Pormetheus config static_config type '''
    targets: list[str]
    labels: dict[LabelName, str] = {}


class RelabelConfig(BaseModel):
    ''' Pormetheus config relabel_config type '''
    source_labels: list[LabelName] | None = None
    separator: str = ';'
    target_label: LabelName
    regex: str = '(.*)'
    modulus: int | None = None
    replacement: str = '$1'
    action: RelabelAction = RelabelAction.REPLACE

    model_config = ConfigDict(use_enum_values=True, validate_default=True)


class ScrapeConfig(BaseModel):
    ''' Pormetheus config scrape_configs setting item scheme '''
    job_name: str
    scrape_interval: Duration | None = None
    scrape_timeout: Duration | None = None
    metrics_path: str = '/metrics'
    honor_labels: bool = False
    honor_timestamps: bool = False
    scheme: Scheme = Scheme.http
    params: dict[str, list[str]] | None = None
    static_configs: list[StaticConfig] | None = None
    http_sd_configs: list[HttpSDConfig] | None = None
    azure_sd_configs: list[AzureSDConfig] | None = None
    gce_sd_configs: list[GceSDConfig] | None = None
    docker_sd_configs: list[DockerSDConfig] | None = None
    relabel_configs: list[RelabelConfig] | None = None
    metric_relabel_configs: list[RelabelConfig] | None = None
    body_size_limit: int = 0
    sample_limit: int = 0
    label_limit: int = 0
    label_name_length_limit: int = 0
    label_value_length_limit: int = 0
    target_limit: int = 0
    keep_dropped_targets: int = 0
    native_histogram_bucket_limit: int = 0
    basic_auth: BasicAuth | None = None
    authorization: Authorization | None = None
    oauth2: Oauth2 | None = None
    follow_redirects: bool = True
    enable_http2: bool = True
    tls_config: TlsConfig | None = None
    proxy_url: str | None = None
    no_proxy: str | None = None
    proxy_from_environment: bool = False
    proxy_connect_header: dict[str, list[str]] | None = None

    model_config = ConfigDict(use_enum_values=True, validate_default=True)


class GlobalConfig(BaseModel):
    ''' Pormetheus config global setting scheme '''
    scrape_interval: Duration = '1m'
    scrape_timeout: str = '10s'
    evaluation_interval: str = '1m'
    external_labels: dict[LabelName, str] = {}
    body_size_limit: Size = 0
    sample_limit: int = 0
    label_limit: int = 0
    label_name_length_limit: int = 0
    label_value_length_limit: int = 0
    target_limit: int = 0
    keep_dropped_targets: int = 0


class Config(BaseModel):
    ''' Pormetheus config scheme '''
    global_: GlobalConfig = Field(..., alias='global')
    scrape_configs: list[ScrapeConfig]

    @classmethod
    def from_file(cls, config_path: str = PROMETHEUS_CONFIG_PATH):
        '''
            Return Config instance populated from Prometheus config file.

            :param config_path: The path to Prometheus configuration directory.
        '''
        with open(os.path.join(config_path, 'prometheus.yml'), 'r') as file:
            return cls(**yaml.load(file, Loader=SafeLoader))

    def save(self, config_path: str = PROMETHEUS_CONFIG_PATH) -> None:
        '''
            Save the configuration to Prometheus config file.

            :param config_path: The path to Prometheus configuration directory.
        '''
        with open(os.path.join(config_path, 'prometheus.yml'), 'w') as file:
            file.write(
                yaml.dump(
                    # mode='json' - required to serialize SecretStr (credentials) fields
                    # by_alias=True - some fields in Prom config file conflict with Python reserved words
                    # exclude_none=True - don't pollute Prom config file with optional fields that are unset.
                    self.model_dump(mode='json', by_alias=True, exclude_none=True),
                    default_flow_style=False
                )
            )

    def search_scrape_config_by_job_name(self, job_name: str) -> ScrapeConfig | None:
        '''
            Return scrape_config setting by a job name or None if the setting doesn't exist.

            :param job_name: Prometheus scrape job name to search the settings by.
        '''
        for scrape_config in self.scrape_configs:
            if job_name == scrape_config.job_name:
                return scrape_config
        return None

    def add_scrape_config(self, scrape_config: ScrapeConfig) -> bool:
        '''
            Add Prometheus scrape_config setting to the list of configured scrape_configs.
            Returns True if the job has been added and False the setting already exists.

            :param scrape_config: Prometheus scrape_config setting to add to the configuration.
        '''
        if self.search_scrape_config_by_job_name(scrape_config.job_name) is not None:
            return False
        self.scrape_configs.append(scrape_config)
        return True

    def remove_scrape_config_by_job_name(self, job_name: str) -> bool:
        '''
            Remove Prometheus srape_settings item by Prometheus job name.
            Returns True if the job has been removed or False if the job doesn't
            exist.

            :param job_name: Prometheus job to remove from the configuration.
        '''
        for index, scrape_config in enumerate(self.scrape_configs):
            if scrape_config.job_name == job_name:
                self.scrape_configs.pop(index)
                return True
        return False


from fastapi import APIRouter, HTTPException

from mmui.config import PROMETHEUS_URL, GRAFANA_URL

from .models import Config, ScrapeConfig, GlobalConfig, PromStatusMessage, StaticConfig
from .utils import reload_prom_query, prom_health_query, prom_readiness_query


router = APIRouter()


@router.get("/config")
async def get_config() -> Config:
    ''' Get Prometheus configuration settings. '''
    return Config.from_file()


@router.post("/config/reset")
async def reset_config() -> PromStatusMessage:
    ''' Set Prometheus configuration to default values. '''
    prom_sc = ScrapeConfig(
        job_name='prometheus',
        metrics_path='prometheus/metrics',
        static_configs=[StaticConfig(targets=[f"{PROMETHEUS_URL.split('//')[1]}"])]
    )
    graf_sc = ScrapeConfig(
        job_name='grafana',
        metrics_path='grafana/metrics',
        static_configs=[StaticConfig(targets=[f"{GRAFANA_URL.split('//')[1]}"])]
    )
    Config(**{'global': GlobalConfig(), 'scrape_configs': [prom_sc, graf_sc]}) \
        .save()
    return reload_prom_query()


@router.put("/config/set")
async def set_config(config: Config) -> PromStatusMessage:
    ''' Set Prometheus configuration settings. '''
    current_config = Config.from_file()
    config.save()
    reload_query_response = reload_prom_query()
    if reload_query_response.status != 200:
        current_config.save()
        reload_prom_query()
        raise HTTPException(
            status_code=409,
            detail=f'Prometheus status code: {reload_query_response.status}. Message: {reload_query_response.message}'
        )
    return reload_query_response


@router.post("/config/scrapes/scrape")
async def add_scrape_config(scrape_config: ScrapeConfig) -> PromStatusMessage:
    ''' Add Prometheus scrape_config setting. '''
    current_config = Config.from_file()
    config = Config.from_file() 
    if not config.add_scrape_config(scrape_config):
        raise HTTPException(status_code=409, detail="Prometheus Job already exists.")
    config.save()
    reload_query_response = reload_prom_query()
    if reload_query_response.status != 200:
        current_config.save()
        reload_prom_query()
        raise HTTPException(
            status_code=409,
            detail=f'Prometheus status code: {reload_query_response.status}. Message: {reload_query_response.message}'
        )
    return reload_query_response


@router.delete("/config/scrapes/scrape")
async def delete_scrape_config(job_name: str) -> PromStatusMessage:
    ''' Remove Prometheus srape_config setting. '''
    config = Config.from_file()
    if not config.remove_scrape_config_by_job_name(job_name):
        raise HTTPException(status_code=409, detail="Prometheus Job doesn't exist.")
    config.save()
    return reload_prom_query()


@router.get("/health")
async def get_health() -> PromStatusMessage:
    ''' Query Prometheus health endpoint. '''
    return prom_health_query()


@router.get("/readiness")
async def get_readiness() -> PromStatusMessage:
    ''' Query Prometheus readines endpoint. '''
    return prom_readiness_query()


@router.post("/reload")
async def reload_service() -> PromStatusMessage:
    ''' Reload Prometheus server. '''
    return reload_prom_query()


import re


def duration(v: str) -> str:
    ''' Validate Prometheus 'duration' type. '''
    pattern = re.compile(r'((([0-9]+)y)?(([0-9]+)w)?(([0-9]+)d)?(([0-9]+)h)?(([0-9]+)m)?(([0-9]+)s)?(([0-9]+)ms)?|0)')
    assert re.fullmatch(pattern, v)
    return v


def labelname(v: str) -> str:
    ''' Validate Prometheus 'labelname' type. '''
    pattern = re.compile(r'[a-zA-Z_][a-zA-Z0-9_]*')
    assert re.fullmatch(pattern, v)
    return v


def size(v: str | int) -> str | int:
    ''' Validate Prometheus 'size' type. '''
    pattern = re.compile(r'^(\d+(?:B|KB|MB|GB|TB|PB|EB)|0)$')
    assert re.fullmatch(pattern, str(v))
    return v


import requests

from mmui.config import PROMETHEUS_URL
from .models import PromStatusMessage


def reload_prom_query() -> PromStatusMessage:
    ''' Send reload query to Prometheus server. '''
    r = requests.post(f'{PROMETHEUS_URL}/prometheus/-/reload')
    return PromStatusMessage(status=r.status_code, message=r.text)


def prom_health_query() -> PromStatusMessage:
    ''' Query Prometheus health endpoint. '''
    r = requests.get(f'{PROMETHEUS_URL}/prometheus/-/healthy')
    return PromStatusMessage(status=r.status_code, message=r.text)


def prom_readiness_query() -> PromStatusMessage:
    ''' Query Prometheus readines endpoint. '''
    r = requests.get(f'{PROMETHEUS_URL}/prometheus/-/ready')
    return PromStatusMessage(status=r.status_code, message=r.text)


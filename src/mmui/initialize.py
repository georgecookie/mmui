import os

from .api.v1.prom import models as prom_models
from .config import (
    DATA_PATH,
    PROMETHEUS_CONFIG_PATH,
    GRAFANA_CONFIG_PATH,
    NGINX_CONFIG_PATH,
    GRAFANA_CONFIG_FILE,
    NGINX_CONFIG_FILE,
    GRAFANA_URL,
    PROMETHEUS_URL
)


def create_dir(dir: str) -> None:
    ''' Create a directory and its parent path if it doesn't exist. '''
    if not os.path.exists(dir):
        os.makedirs(dir)


def initialized(data_path: str = DATA_PATH) -> bool:
    ''' Return True if the application has been initialized. '''
    return os.path.exists(os.path.join(data_path, 'initialized'))


def init_data(data_path: str = DATA_PATH) -> None:
    ''' Initialize data directory for the application. '''
    create_dir(data_path)
    if not os.path.exists(os.path.join(data_path, 'initialized')):
        with open(os.path.join(data_path, 'initialized'), 'x'):
            pass


def init_prom(
    data_path: str = DATA_PATH,
    prometheus_config_path: str = PROMETHEUS_CONFIG_PATH,
    prometheus_url: str = PROMETHEUS_URL,
    grafana_url: str = GRAFANA_URL
) -> None:
    ''' Initialize Prometheus data. '''
    if not initialized(data_path=data_path):
        create_dir(prometheus_config_path)
        prom_sc = prom_models.ScrapeConfig(
            job_name='prometheus',
            metrics_path='prometheus/metrics',
            static_configs=[prom_models.StaticConfig(targets=[f"{prometheus_url.split('//')[1]}"])]
        )
        graf_sc = prom_models.ScrapeConfig(
            job_name='grafana',
            metrics_path='grafana/metrics',
            static_configs=[prom_models.StaticConfig(targets=[f"{grafana_url.split('//')[1]}"])]
        )
        prom_models.Config(**{'global': prom_models.GlobalConfig(), 'scrape_configs': [prom_sc, graf_sc]}) \
            .save(prometheus_config_path)


def init_grafana(
    data_path: str = DATA_PATH,
    grafana_config_path: str = GRAFANA_CONFIG_PATH,
    grafana_config_file: str= GRAFANA_CONFIG_FILE
) -> None:
    ''' Initialize Grafana data. '''
    if not initialized(data_path=data_path):
        create_dir(grafana_config_path)
        with open(os.path.join(grafana_config_path, 'grafana.ini'), 'w') as file:
            file.write(grafana_config_file)


def init_nginx(
    data_path: str = DATA_PATH,
    nginx_config_path: str = NGINX_CONFIG_PATH,
    nginx_config_file: str = NGINX_CONFIG_FILE
) -> None:
    ''' Initialize NGINX data. '''
    if not initialized(data_path=data_path):
        create_dir(nginx_config_path)
        with open(os.path.join(nginx_config_path, 'default.conf'), 'w') as file:
            file.write(nginx_config_file)


def init(
    data_path: str = DATA_PATH,
    prometheus_config_path: str = PROMETHEUS_CONFIG_PATH,
    prometheus_url: str = PROMETHEUS_URL,
    grafana_config_path: str = GRAFANA_CONFIG_PATH,
    grafana_config_file: str = GRAFANA_CONFIG_FILE,
    grafana_url: str = GRAFANA_URL,
    nginx_config_path: str = NGINX_CONFIG_PATH,
    nginx_config_file: str = NGINX_CONFIG_FILE,
):
    ''' Initialize the project. '''
    init_nginx(data_path, nginx_config_path, nginx_config_file)
    init_prom(data_path, prometheus_config_path, prometheus_url, grafana_url)
    init_grafana(data_path, grafana_config_path, grafana_config_file)

    # This has to be run as the last step in order
    # to mark the application as initialized.
    init_data(data_path)


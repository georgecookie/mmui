import uvicorn
from fastapi import FastAPI, HTTPException

from .initialize import initialized, init
from .api.router import router as api_router
from .config import (
    LISTEN_ADDR,
    LISTEN_PORT,
    RELOAD_ON_CHANGE,
    PROXY_SUBPATH
)


app = FastAPI()

app.include_router(prefix='/api', router=api_router)


@app.get("/health", tags=['Management'])
async def check_health() -> dict:
    ''' Return the health status of the service '''
    if initialized():
        return {'status': 'up'}
    else:
        raise HTTPException(status_code=503, detail='Application is not initialized.')


def run() -> None:
    ''' Start the application. '''
    init()
    uvicorn.run('mmui.app:app', host=LISTEN_ADDR, port=int(LISTEN_PORT), reload=bool(RELOAD_ON_CHANGE), root_path=PROXY_SUBPATH)


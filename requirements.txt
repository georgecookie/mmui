fastapi==0.103.2
uvicorn==0.23.2
requests==2.31.0
PyYAML==6.0.1

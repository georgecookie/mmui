MMUI
===============

## Description
MMUI - Monitoring Manager User Interface. This project provides wrapper functionality around `prometheus`, `grafana`, and `nginx` stack.

## Build

```sh
docker compose -f pipeline/build/docker/docker-compose.yml build
```

## Publish

```sh
docker compose -f pipeline/build/docker/docker-compose.yml push
```

## Deploy

#### Locally

```sh
docker compose -f pipeline/build/docker/docker-compose.yml up [-d]
```

> NOTE: When spinning up the project locally with `docker compose`, the very first run will fail.
> This happens because MMUI container needs to initialize the file structure in the shared volumes.
> Stop the containers (`docker compose -f pipeline/... down`) and run `docker compose up` again.

#### Kubernetes

1. Make sure that `mmui` namespace exists in the cluster.

```sh
kubectl create namespace mmui
```

2. Deploy the manifests:

```sh
kubectl apply -k pipeline/deploy/kubernetes/base
```

3. Check the deployed workloads:

```sh
kubectl -n mmui get all
```

4. Create a tunnel from your device to MMUI Gateway.

```sh
kubectl -n mmui port-forward service/mmui 8080:8080
```

## Usage

View MMUI API documentation on

* http://localhost:8080/docs
* http://localhost:8080/redoc

Access Prometheus Endpoint

* http://localhost:8080/prometheus

Access Grafana Endpoint

* http://localhost:8080/grafana


## Development

To run the tests:

1. Create Python virtual environment directory - `python3 -m venv .venv`
2. Activate python vevn - `source .venv/bin/activate`
3. Install additional dev packages - `pip install -e '.[dev]'`
4. Run `docker compose -f ... up`
5. Run the tests - `pytest`

> Exit the virtual environment by executing `deactivate` command or just close the session.

